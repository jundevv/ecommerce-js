import { Col, Card, Icon } from "antd";
import Link from "next/link";
const { Meta } = Card;
const Text = ({ text }) => <span style={{ color: 'black' }}>{text}</span>

export default ({ id, name, subtitle, price, description, photourl }) => (
  <div 
    style={{ marginBottom: "25px" }}
  >
    <Link href={`/product?id=${id}`} as={`/product/${id}`}>
      <a>
          <Card
            hoverable
            cover={<img alt="example" src={photourl} height={"300px"}/>}
          >
            <Meta
              title={<Text text={name} />}
              description={<Text text={description} />}
            />
            <Meta
              style={{marginTop: '5px', fontWeight: 'bold'}}
              description={<Text text={`$${price}`} />}
            />
        </Card>
      </a>
    </Link>
  </div>
)
