import App, { Container } from "next/app";
import React from 'react';
import { ApolloProvider } from "react-apollo";
import withApollo from "../lib/withApollo";
import { Layout } from "antd";
import NProgress from 'nprogress'
import Router from 'next/router'
import convertDataURIToBinary from "../lib/base64ToBinary";

const { Footer } = Layout;

Router.events.on('routeChangeStart', url => {
  console.log(`Loading: ${url}`)
  NProgress.start()
})
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

class DefaultApp extends App {
  static async getInitialProps({Component, router, ctx}) {
    let pageProps = {};
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }
    return { pageProps };
  }

  componentDidMount () {
    if ( "serviceWorker" in navigator && "PushManager" in window ) {
      navigator.serviceWorker
      .register("/sw.js")
      .then(swReg => {
        console.log(`SW registered. ${swReg}`)

        swReg.pushManager.getSubscription().then(subs => {
          if ( subs === null) {
            Notification.requestPermission().then(permission =>{
              if (permission === "granted"){
                swReg.pushManager.subscribe({
                  userVisibleOnly: true,
                  applicationServerKey: convertDataURIToBinary("BNWeZRJfeSmhLwPjQFsbNgcBcAk-sAOR270sk6yVufnD5MD9XwZs1VWH_j58P79gq3Wh5g2-gQJg4M8aL_MNGRs")
                })
                .then(PushSubscriptionObj => {
                  console.log(PushSubscriptionObj)
                })
              }
            });
          } else {
            console.log(JSON.stringify(subs))
            console.log('Subscribed to push manager')
          }
        })
      })
      .catch(error => console.log(`Can't register SW. ${error}`))
    }
  }

  render () {
    const { Component, pageProps, apollo } = this.props;
    return (
      <Container>
        <ApolloProvider client={apollo}>
          <Layout>
            <Component { ...pageProps } />
            <Footer>This is Footer</Footer>
          </Layout>
        </ApolloProvider>
      </Container>
    )
  }
}

export default withApollo(DefaultApp);