import { gql } from "apollo-boost";
import { PRODUCT_FRAGMENT } from "../../fragment";

export const INDEX_QUERY = gql`
  {
    categories {
      id
      createdAt
      name
    }
    onSale: products(where:{onsale:true}){
      ...ProductItems
    }
    allProducts: products {
      ...ProductItems
    }
  }
  ${PRODUCT_FRAGMENT}
`;