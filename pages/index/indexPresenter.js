import Head from "next/head";
import { Layout, Row, Card } from 'antd';
import Header from "../../components/Header";
import Button from "../../components/Button";
import { STORE_NAME } from "../../util";
import ProductCard from "../../components/ProductCard";
import CartButton from "../../components/CartButton";
const { Content } = Layout;


export default ({ data }) => (
  <>
    <Head>
      <title> Home | { STORE_NAME } </title>
    </Head>
    <Header
      centerColumn={
        <h4>{ STORE_NAME }</h4>
      }
      rightColumn={
        <CartButton 
          href="/cart"
          text="Cart"
        />
      }
      leftColumn={
        <Button 
          href="/search"
          text="Search"
          btnIcon={"search"}
        />
      }
    />
    <Content style={{ padding: "0 50px" }}>
      <div
        style={{
          display: "grid",
          gridGap: "10px",
          gridTemplateColumns: "repeat(auto-fit, minmax(100px, 1fr))",
          width: "100%"
        }}
      >
        {data && data.categories && data.categories.map(category => (
          <Button
            key={category.id}
            href={`/category?name=${category.name.toLowerCase()}`}
            hrefAs={`/category/${category.name.toLowerCase()}`}
            text={category.name}
          />
        ))}
      </div>
      <div style={{ paddingTop: "50px" }}>
        {/* {data && data.onSale && data.onSale.length !== 0 && <h2 style={{paddingLeft: '12.5px'}}>On Sale</h2>} */}
        <div
          style={{
            display: "grid",
            gridGap: "10px",
            gridTemplateColumns: "repeat(auto-fill, minmax(250px, 1fr))",
            width: "100%"
          }}
        >
          {data && data.onSale && data.onSale.map(product => (
            <ProductCard 
              key={product.id} 
              id={product.id} 
              name={product.name}
              subtitle={product.name}
              description={product.description}
              price={product.price}
              photourl={product.photo.url}
            />
          ))}
        </div>
      </div>

      {/* <Row gutter={25} style={{ paddingTop: "50px" }}>
        {data && data.allProducts && data.allProducts.length !== 0 && <h2 style={{paddingLeft: '12.5px'}}>All Products</h2>}
        {data && data.allProducts && data.allProducts.map(product => (
          <ProductCard 
            key={product.id} 
            id={product.id} 
            name={product.name}
            subtitle={product.name}
            description={product.description}
            price={product.price}
            photourl={product.photo.url}
          />
        ))}
      </Row> */}
    </Content>
  </>
)
