import Document, { Head, Main, NextScript } from "next/document";


export default class extends Document {
  static async getInitialProps (ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps }
  }

  render () {
    return (
      <html lang="en">
        <Head>
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/antd/3.16.6/antd.min.css" />
          <link rel="stylesheet" type='text/css' href='/static/nprogress.css' />
          <link rel="manifest" href="/static/manifest.json" />
          <meta name="author" content="Jun" />
          <meta name="description" content="Buy for Jun" />
          <meta name="theme-color" content="black" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}