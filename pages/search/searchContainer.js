import SearchPresenter from "./searchPresenter";
import Search from "antd/lib/transfer/search";
import { Query } from "react-apollo";
import { SEARCH_QUERY } from "./searchQueries";


export default class extends React.Component {
  constructor () {
    super()
    this.state = {
      searchTerm: '',
      canSearch: false, // not to search every letter
      searchTimeoutID: null,
    }
  }

  handleChangeSearchTerm = event => {
    this.setState({ canSearch: false })
    this.state.searchTimeoutID !== null && clearTimeout(this.state.searchTimeoutID)
    const { 
      target: { value }
    } = event;

    this.setState({ 
      searchTerm: value 
    })
    const searchTimeoutID = setTimeout(() => {
      this.setState({ canSearch: true })
    }, 1000);
    this.setState({ searchTimeoutID })
  }
  

  render () {
    const { searchTerm, canSearch } = this.state
    return (
      <Query skip={!canSearch} query={SEARCH_QUERY} variables={{ searchTerm }}>
        {({ data }) => (
          <SearchPresenter
            data={data}
            searchTerm={searchTerm} 
            handleChangeSearchTerm={this.handleChangeSearchTerm}
          />
        )}
      </Query>
    )
  }
}
