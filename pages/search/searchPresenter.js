import Head from "next/head";
import { Layout, Row, Card, Input } from 'antd';
import Header from "../../components/Header";
import Button from "../../components/Button";
import { STORE_NAME } from "../../util";
import ProductCard from "../../components/ProductCard";
const { Content } = Layout;


export default ({ data, handleChangeSearchTerm, searchTerm }) => (
  <>
    <Head>
      <title> Search | { STORE_NAME } </title>
    </Head>
    <Header
      centerColumn={
        <h4>{searchTerm === '' ? 'Search' : `Searching by ${searchTerm}`}</h4>
      }
      rightColumn={
        <Button 
          href="/cart"
          text="Cart"
          btnIcon={"shopping-cart"} 
        />
      }
      leftColumn={
        <Button 
          href="/"
          text="Home"
          btnIcon={"home"}
        />
      }
    />
    <Content 
      style={{ padding: "0 50px" }}
    >
      <Input 
        placeholder={"Search by name"}
        value={searchTerm}
        onChange={handleChangeSearchTerm}
      />
      {/* <div
        style={{
          display: "grid",
          gridGap: "10px",
          gridTemplateColumns: "repeat(auto-fit, minmax(100px, 1fr))",
          width: "100%"
        }}
      >
      </div> */}
      <div style={{ paddingTop: "50px" }}>
        {/* {data && data.onSale && data.onSale.length !== 0 && <h2 style={{paddingLeft: '12.5px'}}>On Sale</h2>} */}
        <div
          style={{
            display: "grid",
            gridGap: "10px",
            gridTemplateColumns: "repeat(auto-fill, minmax(250px, 1fr))",
            width: "100%"
          }}
        >
          {data && data.onSale && data.onSale.map(product => (
            <ProductCard 
              key={product.id} 
              id={product.id} 
              name={product.name}
              subtitle={product.name}
              description={product.description}
              price={product.price}
              photourl={product.photo.url}
            />
          ))}
        </div>
      </div>

    </Content>
  </>
)
