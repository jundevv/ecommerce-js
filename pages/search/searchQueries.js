import { gql } from "apollo-boost";
import { PRODUCT_FRAGMENT } from "../../fragment";

export const SEARCH_QUERY = gql`
  query searchQuery($searchTerm: String!) {
    onSale: products(
      where: {
        AND: [
          {
            OR: [
              { name_contains: $searchTerm }
              { description_contains: $searchTerm }
            ]
          }
          { onsale: true }
        ]
      }
    ) {
      ...ProductItems
    }

    allProducts: products(
      where: {
        OR: [
          { name_contains: $searchTerm }
          { description_contains: $searchTerm }
        ]
      }
    ) {
      ...ProductItems
    }
  }
  ${PRODUCT_FRAGMENT}
`;