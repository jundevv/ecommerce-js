import CartPresenter from './cartPresenter';
import { Query } from 'react-apollo';
import { CART_QUERY } from './cartQueries';


export default () => 
  <Query query={CART_QUERY}>
    {({ data }) => <CartPresenter data={data} />}
  </Query>