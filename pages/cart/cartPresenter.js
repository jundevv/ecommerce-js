import Head from "next/head";
import { Layout, Row, Card } from 'antd';
import Header from "../../components/Header";
import Button from "../../components/Button";
import { STORE_NAME } from "../../util";
import ProductCard from "../../components/ProductCard";
const { Content } = Layout;


export default ({ data }) => (
  <>
    <Head>
      <title> Cart | { STORE_NAME } </title>
    </Head>
    <Header
      centerColumn={
        <h4>CART</h4>
      }
      leftColumn={
        <Button 
          href="/"
          text="Home"
          btnIcon={"home"}
        />
      }
      rightColumn={
        <Button 
          href="/search"
          text="Search"
          btnIcon={"search"}
        />
      }
    />
    <Content style={{ padding: "0 50px" }}>
      <div
        style={{
          display: "grid",
          gridGap: "10px",
          gridTemplateColumns: "repeat(auto-fit, minmax(100px, 1fr))",
          width: "100%"
        }}
      >
        {data && data.categories && data.categories.map(category => (
          <Button
            key={category.id}
            href={`/category?name=${category.name.toLowerCase()}`}
            hrefAs={`/category/${category.name.toLowerCase()}`}
            text={category.name}
          />
        ))}
      </div>
      <Row gutter={25} style={{ paddingTop: "50px" }}>
        {data && data.cart && data.cart.length !== 0 && <h2 style={{paddingLeft: '12.5px'}}>On Cart</h2>}
        {data && data.cart && data.cart.map(product => (
          <ProductCard 
            key={product.id} 
            id={product.id} 
            name={product.name}
            subtitle={product.name}
            description={product.description}
            price={product.price}
            photourl={product.photo.url}
          />
        ))}
      </Row>
      <div
        // style={{
        // }}
      >
      {`Total Price: $${
        data && data.cart && data.cart.reduce((totalPrice, product) => (totalPrice + product.price), 0)
      }`}
      </div>
      <Button
        text="Check Out"
        btnIcon="check"
      />
    </Content>
  </>
)
