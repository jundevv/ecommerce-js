import Head from "next/head";
import { Button as AntButton } from "antd";
import Header from "../../components/Header";
import Button from "../../components/Button";
import CartButton from "../../components/CartButton";
import { STORE_NAME } from "../../util";

export default ({ data, toggleCart }) => (
  <>
    <Head>
      <title>{data.product.name} | { STORE_NAME }</title>
    </Head>
    <Header
      centerColumn={<h4>Product</h4>}
      rightColumn={<CartButton href="/cart" text="Cart" />}
      leftColumn={<Button href="/" text="Home" btnIcon={"home"}/>}
    />
    <div className={"product"}>
      <img src={data.product.photo.url} />
      <div>
        <h2>{data.product.name}</h2>
        <h3>{data.product.subtitle}</h3>
        <h4>{data.product.description}</h4>
        <h5>{data.product.size}</h5>
        <h5 className={'out-of-stock'}>{data.product.inventory === 0 ? 'Out of stock' : ''}</h5>
        <h5>${data.product.price}</h5>
        <AntButton type="primary" onClick={toggleCart}>
          {data.product.onCart ? 'Remove from cart': 'Add to cart'}
        </AntButton>
      </div>
      <style jsx>{`
        .product {
          display: grid;
          margin: 50px 0px;
          padding: 0px 50px;
          grid-template-columns: repeat(2, 1fr);
          grid-gap: 50px;
        }
        .product img {
          max-width: 100%;
        }
        .out-of-stock {
          color: #e84118;
        }
      `}</style>
    </div>
  </>
);