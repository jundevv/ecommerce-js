import { gql } from "apollo-boost";
import { PRODUCT_FRAGMENT } from "./fragment";

// import gql from 'graphql-tag';

// export const typeDefs = gql`
//   extend type Query {
//     isLoggedIn: Boolean!
//     cartItems: [ID!]!
//   }

//   extend type Launch {
//     isInCart: Boolean!
//   }

//   extend type Mutation {
//     addOrRemoveFromCart(id: ID!): [Launch]
//   }
// `;

export const defaults = {
  cart: []
}

export const resolvers = {
  Mutation: {
    toggleProduct: (_, variables, { cache, getCacheKey }) => {
      const id = getCacheKey({ __typename: "Product", id: variables.id })
      const fragment = gql`
        ${PRODUCT_FRAGMENT}
      `;
      const product = cache.readFragment({ fragment, id });
      const cartQuery = gql`
        {
          cart @client {
            id
          }
        }
      `
      const { cart } = cache.readQuery({ query: cartQuery });
      const foundProduct = cart.find(x => x.id === product.id)
      let onCart;
      
      if (!foundProduct) {
        cache.writeData({
          data: {
            cart: [ ...cart, product ]
          }
        })
        onCart = true
      }else {
        cache.writeData({
          data: {
            cart: cart.filter(x => x.id !== product.id)
          }
        })
        onCart = false  // onCart actually means 'will be on cart' here.
      }
      cache.writeFragment({
        id: `Product:${product.id}`,
        fragment: PRODUCT_FRAGMENT,
        data: {
          __typename: "Product",
          ...product,
          onCart
        }
      })
    }
  },
  Product: {
    onCart: () => false
  }
};