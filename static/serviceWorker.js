self.addEventListener("install", event => {
  const offlinePage = new Request("/");
  event.waitUntil(
    fetch(offlinePage).then(response => {
      return caches.open("jun-store").then(cache => {
        return cache.put(offlinePage, response)
      })
    })
  )
})

self.addEventListener("fetch", event => {
  event.respondWith(
    fetch(event.request)
    .catch(error => {
      return caches.open("jun-store")
      .then(cache => cache.match("/"))
    })
  )
})

self.addEventListener("push", event => {
  const title = 'Jun Store'
  const options = {
    body: event.data.text(),
    icon: './static/favicon/favicon-192.png',
    image: './static/favicon/favicon-192.png',
    vibrate: [100, 50, 100],
  }
  event.waitUntil(self.registration.showNotification(title, options))
	console.log(event)
})