import withApollo from "next-with-apollo";
import ApolloClient, { InMemoryCache }  from 'apollo-boost'
import { GRAPHQL_URL } from "../config";
import { defaults, resolvers } from "../resolvers";
 

export default withApollo(() => 
  new ApolloClient({
    uri: GRAPHQL_URL,
    clientState: {
      defaults,
      resolvers,
    }
    // cache: new InMemoryCache().restore(initialState || {})
  })
)